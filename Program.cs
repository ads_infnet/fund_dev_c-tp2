﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayManager
{
    class Program
    {
        static List<User> users = new List<User>();
        static int usersCount = users.Count;

        static void Main(string[] args)
        {
            int opt;

            do {
                Console.WriteLine("--- Gerenciador de Aniversários ---");
                Console.WriteLine("[ 1 ] Adicionar pessoas");
                Console.WriteLine("[ 2 ] Pesquisar pessoas");
                Console.WriteLine("[ 0 ] Sair");
                Console.WriteLine("-------------------------------------");
                Console.Write("Selecione uma opção digitando o número correspondente: ");

                opt = Int32.Parse(Console.ReadLine());

                switch (opt)
                {
                    case 1:
                        AddPerson();
                        break;
                    case 2:
                        SearchPerson();
                        break;
                    default:
                        StopProgram();
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
            while (opt != 0);
        }

        private static void StopProgram()
        {
            Console.WriteLine("\nVocê escolheu sair do programa. Fechando...\n");
            Environment.Exit(0);
        }

        private static void AddPerson()
        {
            Console.WriteLine("\nVocê escolheu: Adicionar uma pessoa... \n");

            Console.WriteLine("\nInsira o Primeiro Nome: ");
            string firstName = Console.ReadLine();

            Console.WriteLine("\nInsira o Sobrenome: ");
            string secondName = Console.ReadLine();

            Console.WriteLine("\nInsira a Data de Nascimento (no formato Dia/Mês/Ano): ");
            string inputDate = Console.ReadLine();
            DateTime birthDate;
            DateTime.TryParseExact(inputDate, "dd/MM/yyyy", null, DateTimeStyles.None, out birthDate);

            users.Add(new User { UserID = usersCount++, FirstName = firstName, SecondName = secondName, BirthDate = birthDate });

            //#if DEBUG
            //    Console.WriteLine("\nDEBUG:\nNome: {0} {1} - Data de Nascimento: {2:dd/MMMM/yyyy} - UsersCount: {3}", firstName, secondName, birthDate, usersCount);
            //#endif
            Console.WriteLine("\nOs dados foram inseridos com sucesso. Pressione qualquer tecla para voltar ao menu.");           
        }

        private static void SearchPerson()
        {
            Console.WriteLine("\nVocê escolheu: Buscar uma pessoa...\n");

            // Buscar por: Nome, Sobrenome ou nome completo
            Console.WriteLine("Digite o nome ou parte do nome da pessoa que você está buscando...\n");

            string inputSearch;
            inputSearch = Console.ReadLine().ToLower();

            if (usersCount > 0)
            {

                Console.Write("\nDigite o número correspondente (entre []) ao registro para ver detalhes.\n\n");
                var filteredUsers = users.Where(u => u.FirstName.ToLower().Contains(inputSearch) || u.SecondName.ToLower().Contains(inputSearch));
                foreach (User u in filteredUsers)
                {
                    Console.WriteLine("[{0}] Nome: {1} {2} - Data de Nascimento: {3}", u.UserID, u.FirstName, u.SecondName, u.BirthDate);
                }
                

                string userSelect;
                userSelect = Console.ReadLine();
                OpenItem(userSelect);
                
            } else
            {
                Console.WriteLine("Não há cadastros para fazer a busca.");
            }
        }

        private static void OpenItem(string SelectedUserID)
        {
            Console.Clear();
            Console.WriteLine("\nAbrindo as informações do usuário...\n");

            DateTime today = DateTime.Today;

            int userID = 1;
            Int32.TryParse(SelectedUserID, out userID);
            var selectedUser = users.Where(u => u.UserID == userID);
            foreach (User u in selectedUser)
            {
                int bdDay = u.BirthDate.Day;
                int bdMonth = u.BirthDate.Month;

                DateTime birthDateNow = new DateTime(today.Year, bdMonth, bdDay);
                double daysToBirthDay = birthDateNow.Subtract(today).TotalDays;

                // [1] Nome: leo silva - Data de Nascimento: 01/01/0001 00:00:00
                Console.WriteLine("[{0}] Nome: {1} {2} - Data de Nascimento: {3:dd/MM/yyyy}", u.UserID, u.FirstName, u.SecondName, u.BirthDate);
                Console.WriteLine("Faltam {0} dias para o aniversário de {1} {2}", daysToBirthDay, u.FirstName, u.SecondName);

            }

            Console.WriteLine("\nDigite 0 para voltar ao menu principal.");
        }
    }
}
